package com.kaizen.others.curso;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.EventosTCP;
import com.ks.lib.tcp.Servidor;
import com.ks.lib.tcp.Tcp;
import org.apache.logging.log4j.Marker;

/**
 * Hello world!
 */
public class App
{
    public static void main(String[] args)
    {
        /*
        ClienteTCP cliente = new ClienteTCP();
        cliente.setIP("localhost");

        try
        {
            cliente.setPuerto(5000);
            cliente.conectar();
        }
        catch (Exception ex)
        {
            System.out.println("Error al crear el cliente:" + ex);
        }
        System.out.println("Se termino el programa");
        */

        Tcp conexion;

        if (args[0].equals("cliente"))
        {
            conexion = new Cliente();
            conexion.setIP("localhost");
        }
        else
        {
            conexion = new Servidor();
        }
        try
        {
            conexion.setPuerto(Integer.parseInt(args[1]));
            conexion.setEventos(new ClienteTCP());
            conexion.conectar();
        }
        catch (Exception ex)
        {
            System.out.println("Problema al crear la conexion: " + ex);
        }

    }
}
