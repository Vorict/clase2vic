package com.kaizen.others.curso;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.EventosTCP;
import com.ks.lib.tcp.Servidor;
import com.ks.lib.tcp.Tcp;

public class ClienteTCP implements EventosTCP
{
    public ClienteTCP()
    {

    }

    @Override
    public void conexionEstablecida(Cliente cliente)
    {
        System.out.println("Se conecto");
        cliente.enviar("Mi primer curso");
    }

    @Override
    public void errorConexion(String s)
    {
        System.out.println("Problema con la conexion: " + s);
    }

    @Override
    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        System.out.println("Mensaje recibido: " + s);
    }

    @Override
    public void cerrarConexion(Cliente cliente)
    {
        System.out.println("Se cerro la conexion");
    }
}
