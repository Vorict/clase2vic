package com.kaizen.others.curso.comunicaciones;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.EventosTCP;
import com.ks.lib.tcp.Tcp;

import java.util.LinkedList;

public class Distribuidor extends Cliente implements EventosTCP
{
    private static LinkedList<Distribuidor> lista;

    static
    {
        lista = new LinkedList<>();
    }


    public Distribuidor()
    {
        setEventos(this);
        lista.add(this);
    }

    @Override
    public void conexionEstablecida(Cliente cliente)
    {

    }

    @Override
    public void errorConexion(String s)
    {

    }

    @Override
    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {

    }

    @Override
    public void cerrarConexion(Cliente cliente)
    {

    }

    public static void sendMessage(String message)
    {
        for (Distribuidor cliente : lista)
        {
            cliente.enviar(message);
        }
    }
}
