package com.kaizen.others.curso;

import com.kaizen.others.curso.comunicaciones.ClaseA;
import com.kaizen.others.curso.database.mysql.Clase3;
import com.kaizen.others.curso.comunicaciones.Clase3H;
import com.kaizen.others.curso.database.mysql.Clase4;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main (String args[])
    {
        ClaseA mivar;
        mivar = new ClaseA();
        mivar.escribirNombre("Miguel", "hernandez");

        System.out.println("-----------------------------------\n");

        Clase3 clase1;
        clase1 = new Clase3();
        System.out.println(clase1.getId());

        Clase3 clase2 = new Clase3();
        System.out.println(clase2.getId());

        Clase3 clase3 = new Clase3();
        System.out.println(clase3.getId());

        Clase3 clase4 = new Clase3();
        System.out.println(clase4.getId());

        Clase3 clase5 = new Clase3();
        System.out.println(clase5.getId());

        System.out.println("-----------------------------------\n");

        clase3.setId(10);

        System.out.println("El total de clases creadas es de: " + Clase3.getContador());


        System.out.println(clase1.getId());
        System.out.println(clase2.getId());
        System.out.println(clase3.getId());
        System.out.println(clase4.getId());
        System.out.println(clase5.getId());

        System.out.println("-----------------------------------\n");

        Clase4 clasesuma = new Clase4();
        clasesuma.addContadorAntiguio(5);

        System.out.println(clase1.getId());
        System.out.println(clase2.getId());
        System.out.println(clase3.getId());
        System.out.println(clase4.getId());
        System.out.println(clase5.getId());

        System.out.println("-----------------------------------\n");

        clasesuma.addContadorNuevo(100);

        System.out.println(clase1.getId());
        System.out.println(clase2.getId());
        System.out.println(clase3.getId());
        System.out.println(clase4.getId());
        System.out.println(clase5.getId());


        System.out.println("-----------------------------------\n");


        Clase3H herencia = new Clase3H();
        System.out.println(herencia.getId());

        clasesuma.addContadorNuevo(100);

        System.out.println(clase1.getId());
        System.out.println(clase2.getId());
        System.out.println(clase3.getId());
        System.out.println(clase4.getId());
        System.out.println(clase5.getId());
        System.out.println(herencia.getId());


    }
}
